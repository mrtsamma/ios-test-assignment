# README #

iOS Test Assignment

---

## Description ##

The app displays soccer competitions data from:
https://api.football-data.org/v2/competitions

---

This will be a simple app that will show on the first screen 4 rows.
Tap on a row will lead to the next page where it will show precise data.

1. Competitions count.  
    On the next page display 
    * The count
2. The country where occurred the most competitions.  
    On the next page display:
    * Country name which described in Area.
    * Country name in the language of the country, if possible. If not, then won't display it at all.  
    F.e:  
    "area": { "name": "Finland", "countryCode": "FIN" }  
    Country name which described in Area: "Finland".  
    In based on country the name will be: "Suomi".  
3. The most seasons owning(numberOfAvailableSeasons) competition.  
    On the next page display:
    * Name
    * Start date
    * End date
    * lastUpdated date
4. Competitions count per year.  
    On the next page display:  
    * A year and competitions count

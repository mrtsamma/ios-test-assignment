//
//  Competitions.swift
//  iOSTest
//
//  Created by Märt Samma on 17.02.2020.
//  Copyright © 2020 Märt Samma. All rights reserved.
//

import Foundation

struct Competitions: Decodable {
    let count: Int
    let competitions: [Competition]
}

struct Competition: Decodable {
    let id: Int
    let name: String
    let area: Area
    let currentSeason: CurrentSeason?
    let numberOfAvailableSeasons: Int
    let lastUpdated: Date // format in API: "2018-06-04T23:54:04Z"
}

struct Area: Decodable {
    let name: String
    let countryCode: String
}

struct CurrentSeason: Decodable {
    let startDate: Date
    let endDate: Date //"2021-11-16"

    enum CurrentSeasonError: Error {
        case startDateParsingFailure
        case endDateParsingFailure
    }

    private enum CodingKeys: String, CodingKey {
        case startDate
        case endDate
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let startDateString = try container.decode(String.self, forKey: .startDate)
        let endDateString = try container.decode(String.self, forKey: .endDate)

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let startDate = formatter.date(from: startDateString) else {
            throw CurrentSeasonError.startDateParsingFailure
        }

        guard let endDate = formatter.date(from: endDateString) else {
            throw CurrentSeasonError.endDateParsingFailure
        }

        self.startDate = startDate
        self.endDate = endDate
    }
}

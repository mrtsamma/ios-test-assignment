//
//  MainViewController.swift
//  iOSTest
//
//  Created by Märt Samma on 18.02.2020.
//  Copyright © 2020 Märt Samma. All rights reserved.
//

import UIKit

final class MainViewController: UIViewController {
    private let cellReuseId = String(describing: UITableViewCell.self)

    override func loadView() {
        super.loadView()

        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseId)

        view = tableView
        tableView.delegate = self
        tableView.dataSource = self
    }

    private func fetchCompetions() {
        guard let url = URL(string: "https://api.football-data.org/v2/competitions") else {
            return
        }
        let api = APIClient(dateDecodingStrategy: .iso8601)
        api.makeGETRequest(to: url, responseType: Competitions.self) { (result: Result<Competitions, Error>) in
        }
    }
}

// MARK: - UITableViewDataSource
extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId, for: indexPath)
        // TODO:
        return cell
    }
}

// MARK: - UITableViewDelegate
extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // TODO:
    }
}

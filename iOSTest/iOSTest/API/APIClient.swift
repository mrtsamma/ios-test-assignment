//
//  File.swift
//  iOSTest
//
//  Created by Märt Samma on 18.02.2020.
//  Copyright © 2020 Märt Samma. All rights reserved.
//

import Foundation

enum APIError: Error {
    case noResponseData
}

protocol Cancallable {
    func cancel()
}

extension URLSessionDataTask: Cancallable {}

struct APIClient {
    var dateDecodingStrategy: JSONDecoder.DateDecodingStrategy?

    @discardableResult
    func makeGETRequest<ResponseType: Decodable>(
        to url: URL,
        responseType: ResponseType.Type,
        completion: @escaping (Result<ResponseType, Error>) -> Void
    ) -> Cancallable {
        let task = URLSession.shared.dataTask(with: url) {
            [dateDecodingStrategy] (data, response, error) in

            let result: Result<ResponseType, Error> = Result {
                if let error = error { throw error }
                guard let data = data else { throw APIError.noResponseData }

                let decoder = JSONDecoder()
                dateDecodingStrategy.map({ decoder.dateDecodingStrategy = $0 })
                return try decoder.decode(ResponseType.self, from: data)
            }
            completion(result)
        }
        task.resume()
        return task
    }
}

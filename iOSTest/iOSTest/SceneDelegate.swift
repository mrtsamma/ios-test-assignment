//
//  SceneDelegate.swift
//  iOSTest
//
//  Created by Märt Samma on 17.02.2020.
//  Copyright © 2020 Märt Samma. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func scene(
        _ scene: UIScene,
        willConnectTo session: UISceneSession,
        options connectionOptions: UIScene.ConnectionOptions
    ) {
        guard let windowScene = (scene as? UIWindowScene) else { return }

        let window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window.windowScene = windowScene
        // TODO: Update with correct VC
        let viewController = MainViewController()
        window.rootViewController = viewController
        window.makeKeyAndVisible()

        self.window = window
    }
}
